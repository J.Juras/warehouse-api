const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;

// show all products, with optional filtering and sorting
recordRoutes.route("/products").get(async function(req, res) {
    try {
        let db_connect = dbo.getDb("warehouse");
        let { sortBy, order, filterBy, filterValue } = req.query;
        let sortQuery = {};
        let filterQuery = {};

        // sorting
        if (sortBy && order) {
            sortQuery[sortBy] = order === 'asc' ? 1 : -1;
        }

        // filtering
        if (filterBy && filterValue) {
            filterQuery[filterBy] = filterValue;
        }

        let result = await db_connect.collection("products")
            .find(filterQuery)
            .sort(sortQuery)
            .toArray();

        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: "An error occurred while fetching products" });
    }
});

// show one product with a specific id
recordRoutes.route("/products/:id").get(async function(req, res) {
    try {
        let db_connect = dbo.getDb("warehouse");
        let myquery = { _id: ObjectId(req.params.id) };
        let result = await db_connect.collection("products").findOne(myquery);
        
        res.json(result);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: "An error occurred while fetching the product" });
    }
});

// add a product
recordRoutes.route("/products").post(async function(req, response){
    try {
        let db_connect = dbo.getDb("warehouse");
        let { name, price, description, quantity, unit } = req.body;

        let existingProduct = await db_connect.collection("products").findOne({ name });
        if (existingProduct) {
            return response.status(400).json({ error: 'Product with this name already exists' });
        }

        let myobj = { name, price, description, quantity, unit };
        let res = await db_connect.collection("products").insertOne(myobj);

        console.log("1 product added successfully")
        response.json(res);
    } catch (err) {
        console.error(err);
        response.status(500).json({ error: "An error occurred while adding the product" });
    }
});

// update a product
recordRoutes.route("/products/:id").put(async function(req, response){
    try {
        let db_connect = dbo.getDb("warehouse");
        let myquery = { _id: ObjectId(req.params.id) };
        let { name, price, description, quantity, unit } = req.body;

        let newValues = {
            $set: { name, price, description, quantity, unit }
        };
        
        let res = await db_connect.collection("products").updateOne(myquery, newValues);

        console.log("1 product updated successfully");
        response.json(res);
    } catch (err) {
        console.error(err);
        response.status(500).json({ error: "An error occurred while updating the product" });
    }
});

// delete a product
recordRoutes.route("/products/:id").delete(async function (req, res) {
    try {
        let db_connect = dbo.getDb("warehouse");
        let myquery = { _id: ObjectId(req.params.id) };

        let existingProduct = await db_connect.collection("products").findOne(myquery);
        if (!existingProduct) {
            return res.status(400).json({ error: 'Product not found' });
        }

        let obj = await db_connect.collection("products").deleteOne(myquery);

        console.log("1 product deleted");
        res.json(obj);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: "An error occurred while deleting the product" });
    }
});

// generate a report
recordRoutes.route("/report").get(async function (req, res) {
    try {
        let db_connect = dbo.getDb("warehouse");
        let result = await db_connect.collection("products").aggregate([
            {
                $group: {
                    _id: null,
                    products: {
                        $push: {
                            name: '$name',
                            quantity: '$quantity',
                            totalValue: { $multiply: ['$price', '$quantity'] }
                        }
                    },
                    totalQuantityAllProducts: { $sum: '$quantity' },
                    totalValueAllProducts: { $sum: { $multiply: ['$price', '$quantity'] } }
                }
            },
            {
                $project: {
                    _id: 0,
                    products: 1,
                    totalQuantityAllProducts: 1,
                    totalValueAllProducts: 1
                }
            }
        ]).toArray();

        res.json(result[0]);
    } catch (err) {
        console.error(err);
        res.status(500).json({ error: "An error occurred while generating the report" });
    }
});

module.exports = recordRoutes;
