const express = require('express');
const app = express();
const cors = require('cors');
require("dotenv").config({path: "./config.env"});
const port = process.env.PORT || 5000;
app.use(cors());
app.use(express.json());
app.use(require("./routes/record"));

const dbo = require("./db/conn");

const startServer = async () => {
    try {
        await dbo.connectToServer();
        app.listen(port, () => {
            console.log(`Server is running on ${port}`);
        });
    } catch (err) {
        console.error("Error starting the server:", err);
        process.exit(1);
    }
};

startServer();