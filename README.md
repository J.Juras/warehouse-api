# REST API project for databases class
**REST API for a warehouse, using Express.js and MongoDB.**

# Running the app:
1. there must be a Mongo database called "warehouse" running (using Docker for example)
2. enter the project directory and use the command:
```bash
node server.js
```
3. the server will be available on https://localhost:5000

## Endpoints

### 1. Get All Products
**URL:** `GET /products`  
**Description:** Retrieves all products with optional filtering and sorting.  
**Query Parameters:**  
- `sortBy`: Field to sort by (e.g., `price`, `name`).  
- `order`: Sorting order (`asc` for ascending, `desc` for descending).  
- `filterBy`: Field to filter by (e.g., `name`, `unit`).  
- `filterValue`: Value for filtering.  

**Example Request:**  
```sh
GET /products?sortBy=price&order=asc&filterBy=unit&filterValue=kg
```

---

### 2. Get a Single Product
**URL:** `GET /products/:id`  
**Description:** Retrieves a product by its unique ID.  

**Example Request:**  
```sh
GET /products/65a3f4e7c5b5a3d2a7e10b45
```

---

### 3. Add a New Product
**URL:** `POST /products`  
**Description:** Adds a new product to the inventory.  
**Request Body (JSON):**  
```json
{
  "name": "Product Name",
  "price": 100,
  "description": "Product Description",
  "quantity": 50,
  "unit": "kg"
}
```
**Response:**  
- Returns an error if a product with the same name already exists.

---

### 4. Update an Existing Product
**URL:** `PUT /products/:id`  
**Description:** Updates product details using its unique ID.  
**Request Body (JSON):**  
```json
{
  "name": "Updated Name",
  "price": 120,
  "description": "Updated Description",
  "quantity": 60,
  "unit": "kg"
}
```

---

### 5. Delete a Product
**URL:** `DELETE /products/:id`  
**Description:** Deletes a product by its ID.  
**Response:**  
- Returns an error if the product does not exist.

---

### 6. Generate Report
**URL:** `GET /report`  
**Description:** Generates a report summarizing all products, including:  
- Each product's name, quantity, and total value (`price * quantity`).  
- The total quantity of all products.  
- The total inventory value.

**Example Response:**
```json
{
  "products": [
    { "name": "Product A", "quantity": 50, "totalValue": 5000 },
    { "name": "Product B", "quantity": 20, "totalValue": 2000 }
  ],
  "totalQuantityAllProducts": 70,
  "totalValueAllProducts": 7000
}
```